
//Adding new collection of users
db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations"
		},
		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 21,
			"email": "janedoe@mail.com",
			"department": "HR"
		}
	])


//finding users with s in firstname or d in lastname 

db.users.find(
	{
		$or: [
			{
				"firstName": {
					$regex: 's',
					$options: '$i'
				}
			},
			{
				"lastName": {
					$regex: 'd',
					$options: '$i'
				}
			}
		]
	},
	{
			"_id": 0,
			"firstName": 1,
			"lastName": 1
	}
)

//finding users who are from the HR Department and their age is greater than or equal to 70 

db.users.find(
{	
	$and: [
		{
			"department": "HR"
		},
		{
			"age": {
				$gte: 70
			}
		}
	]
}
)

//find users with the letter e in their first name and has an age of less than or equal to 30

db.users.find({
	$and: [
		{
			"firstName": {
				$regex: 'e',
				$options: '$i'
			}
		},
		{
			"age": {
				$lte: 30
			}
		}
	]
})