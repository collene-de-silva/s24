//adding users
db.users.insertMany([
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"email": "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"email": "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firrelli",
			"email": "jfirrelli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"email": "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"email": "pcastillo@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"email": "gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}
	])

//adding courses
db.courses.insertMany([
		{
			"name": "Professional Development",
			"price": 10000.0
		},
		{
			"name": "Business Processing",
			"price": 13000.0
		},
	])

//finding users who are not administrators 
db.users.find({"isAdmin": false})


//adding userIds
db.courses.updateMany(
		{},
		{
			$set: {
					"enrollees": [
						{
							"userId": ObjectId("620cc660fabc327c59e5effd")
						},
						{
							"userId": ObjectId("620cc660fabc327c59e5effe")
						},
						{
							"userId": ObjectId("620cc660fabc327c59e5efff")
						},
						{
							"userId": ObjectId("620cc660fabc327c59e5f000")
						}
					]
				}
		}
	)